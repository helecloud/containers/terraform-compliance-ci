# pull base image
FROM alpine:3.12

RUN apk --no-cache add \
        python3\
        py3-pip \
        openssl \
        ca-certificates \
        git && \
    apk --no-cache add --virtual build-dependencies \
        python3-dev \
        libffi-dev \
        openssl-dev \
        libxml2-dev \
        libxslt-dev \
        build-base && \
    pip3 install --upgrade pip cffi && \
    pip3 install terraform-compliance && \
    pip3 install --upgrade radish radish-bdd && \
    pip3 install --upgrade pywinrm && \
    apk del build-dependencies && \
    rm -rf /var/cache/apk/*

COPY --from=hashicorp/terraform /bin/terraform /usr/bin/terraform

USER 0

WORKDIR /

CMD [ "/bin/sh" ]
